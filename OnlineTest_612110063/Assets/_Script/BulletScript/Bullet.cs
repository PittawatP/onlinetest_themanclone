﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviourPun
{
    public float BulletSpeed = 20f;
    public PunShootingController Owner { get; private set; }
    [SerializeField]
    Rigidbody2D rb;

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + transform.right * BulletSpeed * Time.fixedDeltaTime);
    }

    public void SetOwner(PhotonView OwnerView)
    {
        Owner = OwnerView.GetComponent<PunShootingController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Debug.Log("Bullet Collision to other player.");

                PunUserNetControl tempOther = collision.gameObject.GetComponent<PunUserNetControl>();
                if (tempOther != null)
                    Debug.Log("Attack to other ViewID : " + tempOther.photonView.ViewID);

                PunHealth tempHealthOther = collision.gameObject.GetComponent<PunHealth>();

                if (tempHealthOther != null)
                {
                    tempHealthOther.TakeDamage(20, photonView.ViewID);

                }
            }
            Destroy(this.gameObject);
        }
    }
}
