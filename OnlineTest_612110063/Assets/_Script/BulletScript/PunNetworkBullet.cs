﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonTransformView))]
[RequireComponent(typeof(PhotonRigidbody2DView))]

public class PunNetworkBullet : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    public PunUserNetControl Owner;
    public int m_AmountDamage = 20;
    
    public float BulletForce = 20f;
    int OwnerViewID = 1;

    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        //store this gameobject as this player's character in player .tagobject
        info.Sender.TagObject = this.gameObject;

        OwnerViewID = info.photonView.ViewID;

        

        Rigidbody2D bullet = GetComponent<Rigidbody2D>();

        bullet.velocity = bullet.transform.right * BulletForce;

        if (!photonView.IsMine)
            return;

        Destroy(this.gameObject, 10.0f);
    }

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!photonView.IsMine)
        {
            return;
        }

        if(collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Bullet Collision to other player.");

            PunUserNetControl tempOther = collision.gameObject.GetComponent<PunUserNetControl>();
            if (tempOther != null)
            {
                Debug.Log(Owner.photonView.ViewID + "Attack to : " + tempOther.photonView.ViewID);
                
            }
                

            PunHealth tempHealthOther = collision.gameObject.GetComponent<PunHealth>();
            
            if (tempHealthOther != null)
            {
                tempHealthOther.TakeDamage(m_AmountDamage, Owner.photonView.ViewID);
            }
        }
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if(!photonView.IsMine)
        {
            return;
        }
        PhotonNetwork.Destroy(this.gameObject);
    }
}
