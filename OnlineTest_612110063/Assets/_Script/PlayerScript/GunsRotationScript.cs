﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsRotationScript : MonoBehaviour
{
    public GameObject bulletPrefabs;
    public Camera cam;
    public float BulletSpeed = 20f;
    public Rigidbody2D bullet;
    Vector2 mousepos;

    private void Awake()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
    

    // Update is called once per frame
    void Update()
    {
        mousepos = cam.ScreenToWorldPoint(Input.mousePosition);
    }
    private void FixedUpdate()
    {
        Vector2 AimDir = mousepos - bullet.position;
        float angle = Mathf.Atan2(AimDir.y, AimDir.x) * Mathf.Rad2Deg - 90f;
        bullet.rotation = angle;
    }
}
