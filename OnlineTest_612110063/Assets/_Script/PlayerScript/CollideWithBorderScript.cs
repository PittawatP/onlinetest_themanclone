﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CollideWithBorderScript : MonoBehaviourPun
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("BorderRight"))
        {
            Debug.Log("Right");
            Vector2 Direction2 = new Vector2(-17f, this.transform.position.y);
            this.gameObject.transform.position = Direction2;
        }

        if (collision.gameObject.CompareTag("BorderLeft"))
        {
            Debug.Log("Left");
            Vector2 Direction = new Vector2(17f, this.transform.position.y);
            this.gameObject.transform.position = Direction;
        }
    }
}
