﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PunHealth : MonoBehaviourPunCallbacks , IPunObservable
{
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    
    public GameObject[] RespawnPoint;
    public int Life = 5;
    public bool IsDead = false;

    [SerializeField] 
    public HealthBar healthbar;
    

    public void OnGUI()
    {
        if (photonView.IsMine)
        {
            GUI.Label(new Rect(0, 100, 300, 50), "Player Health : " + currentHealth);
            GUI.Label(new Rect(0, 200, 300, 50), "Player Life : " + Life);

        }
            
    }

    public void TakeDamage(int amount , int OwnerNetID)
    {
        if(photonView != null)
        {
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        }
        else
        {
            print("photonView is NULL");
        }
    }

    

    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Debug.Log("Take Damage");
        currentHealth -= amount;
        
        


        if (currentHealth <= 0)
        {
            Debug.Log("NetID : " + OwnerNetID.ToString() + " Killed " + photonView.ViewID);
            photonView.RPC("PunResetPlayer", photonView.Owner);
            Life--;
        }
    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(currentHealth);
        }
        else
        {
            currentHealth = (int)stream.ReceiveNext();
        }
    }


    [PunRPC]
    public void PunResetPlayer()
    {
        if(Life >0)
        {
            int i = Random.Range(0, RespawnPoint.Length);
            Debug.Log("Reset Position to " + i);
            Vector3 Direction = RespawnPoint[i].transform.position;
            this.gameObject.transform.position = Direction;
            currentHealth = maxHealth;
        }
        if(Life <= 0)
        {
            Destroy(this.gameObject);
        }
        
        
    }


    public void Healing(int amount)
    {
        currentHealth += amount;
    }

    private void Update()
    {
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    //public void ScoreUp(int KillerID, bool dead)
    //{
    //    photonView.RPC("PunScoreUp", RpcTarget.All, KillerID, true);
    //}

    //[PunRPC]
    //public void PunScoreUp(int KillerID , bool dead)
    //{
    //    Debug.Log("Dead:" + dead + "KillerId : " + KillerID + "View ID" + photonView.ViewID);
    //    if (dead)
    //    {

    //        if (KillerID == photonView.ViewID)
    //        {

    //            Score++;
    //            dead = false;
    //        }
    //    }
    //}

    private void OnDestroy()
    {
        if (!photonView.IsMine)
        {
            return;
        }
        PhotonNetwork.Destroy(this.gameObject);
    }
}
