﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PunShootingController : MonoBehaviourPun
{
    
    public GameObject bulletPrefabs;
    //public Camera cam;
    
    public float BulletSpeed = 20f;
    
    

    
    
    
    // Update is called once per frame
    void Update()
    {
        if (!PunNetworkManager.singleton.isGameStart)
        {
            return;
        }
        if (!photonView.IsMine)
            return;

        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Fire();
        }

        
    }



    void Fire()
    {
        object[] data = { photonView.ViewID };

        Rigidbody2D bullet
            = PhotonNetwork.Instantiate(this.bulletPrefabs.name
            , this.transform.position + (this.transform.right * 1.5f)
            , transform.rotation
            , 0
            , data).GetComponent<Rigidbody2D>();
        //AddVelocity to bullet
        bullet.velocity = bullet.transform.right * BulletSpeed;

        bullet.GetComponent<PunNetworkBullet>().Owner = this.GetComponent<PunUserNetControl>();
    }

    [PunRPC]
    void RPCFire()
    {
        

    }
}
