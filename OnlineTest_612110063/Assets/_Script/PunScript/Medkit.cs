﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Medkit : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    public int healingPoint = 60;
    private bool IsDestroyed;


    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.Sender.TagObject = this.gameObject;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.CompareTag("Player"))
        {
            PunHealth otherHeal = collision.gameObject.GetComponent<PunHealth>();
            otherHeal.Healing(healingPoint);
            IsDestroyed = true;
            photonView.RPC("PunRPCHealing", RpcTarget.MasterClient);
            
        }
    }

    [PunRPC]
    private void PunRPCHealing()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if(!photonView.IsMine)
        {
            return;
        }

        PhotonNetwork.Destroy(this.gameObject);
    }
    private void Update()
    {
        if(!IsDestroyed)
        {
            Destroy(this.gameObject, 10.0f);
        }
    }


}
