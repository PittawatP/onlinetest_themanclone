﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;
using Photon.Realtime;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback
{
    public static GameObject LocalPlayerInstance;
    public Sprite MySprite;
    public int tempScore;
    

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        if(photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
            
            GetComponent<SpriteRenderer>().sprite = MySprite;
        }
        else
        {
            
            //GetComponentInChildren<Camera>().enabled = false;
            //GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<PlayerMovement>().enabled = false;
            GetComponent<PunShootingController>().enabled = false;
        }
    }

    
}
