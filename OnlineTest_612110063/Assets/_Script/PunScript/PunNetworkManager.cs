﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using ExitGames.Client.Photon;

public class PunNetworkManager : ConnectAndJoinRandom, IOnEventCallback
{

    public static PunNetworkManager singleton;

    [Header("Spawn Info")]
    [Tooltip("The prefabs to use for represent the player")]
    public GameObject GamePlayerPrefabs;
    public GameObject MedkitPrefab;

    public GameObject[] MedkitSpawnpoint;
    public GameObject[] RespawnPoint;
    public GameObject[] playerCount;

    [SerializeField]
    private int playerNum = 1;
    
    
    public bool isGameStart;

    bool isFirstSetting = false;
    public int numberOfHealing = 5;
    float m_count = 10;
    public float m_countDownDropHeal = 10;


    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;

    public delegate void GameStart();
    public static event GameStart OnGameStart;

    public delegate void GameOver();
    public static event GameOver OnGameOver;

    private readonly byte isGameOver = 10;


    public Text statusText;
    


    private void Awake()
    {
        singleton = this;

        OnPlayerSpawned  += SpawnPlayer;
        OnFirstSetting += SpawnMedkit;
        OnGameStart += PlayerNumberCount;
        OnGameOver += WhenGameOver;
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        OnPlayerSpawned();
        GetStartGame(PhotonNetwork.CurrentRoom.CustomProperties);
        playerNum = PhotonNetwork.CountOfPlayersInRooms + playerNum;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        GetStartGame(PhotonNetwork.CurrentRoom.CustomProperties);
        playerNum++;
    }

    public void SpawnPlayer()
    {
        
        int j = Random.Range(0, RespawnPoint.Length);
        Vector2 Direction = RespawnPoint[j].transform.position;
        PhotonNetwork.Instantiate(GamePlayerPrefabs.name, Direction, Quaternion.identity, 0);
        Debug.Log("Spawn at " + j);
        
    }

    public void SpawnMedkit()
    {
        if(isFirstSetting == false)
        {
            isFirstSetting = true;
        }
        else if (GameObject.FindGameObjectsWithTag("Medkit").Length < numberOfHealing)
        {
            m_count -= Time.deltaTime;
            if (m_count <= 0)
            {
                m_count = m_countDownDropHeal;
                int i = Random.Range(0, MedkitSpawnpoint.Length);
                PhotonNetwork.Instantiate(MedkitPrefab.name, MedkitSpawnpoint[i].transform.position, Quaternion.identity, 0);
                Debug.Log("Spawn Medkit at " + i);
            }

        }
    }
    private void Update()
    {
        
        playerCount = GameObject.FindGameObjectsWithTag("Player");
        if (PhotonNetwork.IsMasterClient != true)
        {
            if(PhotonNetwork.InRoom)
            {
                GetStartGame(PhotonNetwork.CurrentRoom.CustomProperties);
            }
            
            return;
        }

        GetGameover(PhotonNetwork.CurrentRoom.CustomProperties);

        if (playerNum < 3)
        {
            DisplayMessage("Wait For player");
        }
        else
        {
            DisplayMessage("");
        }
        if (playerNum >= 2)
        {
            OnGameStart();
        }
        
        if (isGameStart == true)
        {
            if (isFirstSetting == false)
            {
                OnFirstSetting();
                
            }

            if (isFirstSetting == true)
            {
                SpawnMedkit();
            }

            if (playerCount.Length == 1)
            {
                CallRaiseEvent();
            }

        }

    }

    #region waittingPlayer
    public void PlayerNumberCount()
    {
        Hashtable props = new Hashtable {
            {PunPlayerSetting.Game_Start,true }
        };

        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }
    public void GetStartGame(Hashtable propertiesThatChanged)
    {
        object startGameFromProps;

        if (propertiesThatChanged.TryGetValue(PunPlayerSetting.Game_Start, out startGameFromProps))
        {
            isGameStart = (bool)startGameFromProps;
        }
    }
    #endregion

    public void WhenGameOver()
    {
        Hashtable props = new Hashtable {
            {PunPlayerSetting.Game_Over,true }
        };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }
    public void GetGameover(Hashtable propertiesThatChanged)
    {
        object startGameFromProps;

        if (propertiesThatChanged.TryGetValue(PunPlayerSetting.Game_Over, out startGameFromProps))
        {
            isGameStart = (bool)startGameFromProps;
        }
    }


    private void OnEnable()
    {
        base.OnEnable();

        PhotonNetwork.AddCallbackTarget(this);
    }
    private void OnDisable()
    {
        base.OnDisable();

        PhotonNetwork.RemoveCallbackTarget(this);
    }

    private void CallRaiseEvent()
    {
        object[] content = new object[] { isGameOver };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        SendOptions sendOptions = new SendOptions { Reliability = true, Encrypt = true };

        PhotonNetwork.RaiseEvent(isGameOver, content, raiseEventOptions, sendOptions);
        Debug.Log("Call RaiseEvent");
    }

    

    public void OnEvent(EventData photonEvent)
    {

        Debug.Log(photonEvent.ToStringFull());

        byte eventCode = photonEvent.Code;
        if(eventCode == isGameOver)
        {
            Debug.Log("Call Raise event is : " + eventCode.ToString());

            object[] data = (object[])photonEvent.CustomData;

            DisplayMessage("Game Over");
            Debug.Log("GameOver");
            isGameStart = false;
        }
    }
    
    public void DisplayMessage(string message)
    {
        statusText.text = message;
        
    }
}
